package br.ucsal.bes20182.poo.atividade04;

import java.util.Scanner;

public class Atividade04 {

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		int contatos;

		System.out.println("Qual o tamanho desejado da agenda?");
		contatos = sc.nextInt();

		String[] nome = new String[contatos];
		String[] telefone = new String[contatos];
		Integer[] anoNasc = new Integer[contatos];

		for (int i = 0; i < nome.length; i++) {
			nome[i] = "";
			telefone[i] = "";
			anoNasc[i] = 0;

		}

		int opcao = 0;
		int posicao = 0;

		String continuar = "";
		String nomeExcluir = ""; 
		String nomePesquisa = "";

		do {

			System.out.println("Voce esta na AGENDA JAVA com capacidade para "+ contatos + " contatos," + "\n" + 
			        "voce deseja:" + "\n" +
					"[1] Incluir contato" + "\n" +
					"[2] Listar contatos adicionados" + "\n"+
					"[3] Excluir contato"  + "\n" +
					"[4] Pesquisar entre os contatos"  + "\n" +
					"[5] Finalizar a agenda criada");

			opcao = sc.nextInt(); 
			sc.nextLine();

			switch (opcao) {
			case 1:
				// Codigo para Incluir

				if (posicao <= nome.length) {					
					do {

						System.out.print("Digite o nome: ");
						nome[posicao] = sc.nextLine();

						System.out.print("Digite o telefone: ");
						telefone[posicao] = sc.nextLine();

						System.out.print("Digite o ano de Nascimento: ");
						anoNasc[posicao] = sc.nextInt();
						sc.nextLine();

						System.out.print("\n" + "Deseja continuar o cadastramento? 1-Sim  2-Não " + "\n");
						continuar = sc.nextLine();

						posicao++;

					} while (continuar.equals("1"));

				} else {

					System.out.println("Sua agenda esta cheia!");

				}


				break;

			case 2:
				// Codigo que lista todos os contatos
				for (int i = 0; i < nome.length; i++) {

					if (!nome[i].equals("")) {

						System.out.println("Nome: " + nome[i] + " Telefone: "
								+ telefone[i] + " Ano de Nascimento: " + anoNasc[i]);

					}

				}
				break;

			case 3:
				// Codigo para Excluir
				System.out.println("Quem deseja excluir? ");
				nomeExcluir = sc.nextLine();

				for (int i = 0; i < nome.length; i++) {
					if (nome[i].equals(nomeExcluir)) {

						nome[i] = "";
						telefone[i] = "";
						anoNasc[i] = 0;
					}

				}
				break;

			case 4:	
				//Codigo para pesquisar entre os contatos 
				System.out.println("Digite o nome para pesquisa: ");
				nomePesquisa = sc.nextLine();

				for (int i = 0; i < telefone.length; i++) {

					if (nome[i].equalsIgnoreCase(nomePesquisa)) {

						System.out.println("Nome: " + nome[i] + 
								"Telefone: " + telefone[i] + 
								"Ano de Nascimento: " + anoNasc[i]);							
					}

				}

				break;

			case 5:
				// Codigo para finalizar a agenda
				System.out.println("Agenda Finalizada!");
				return;

			default:
				// Codigo para seleção de uma opção indisponivel
				System.out.println("Operação Invalida!");
				break;
			}

		} while (opcao != 5);

	}

}
